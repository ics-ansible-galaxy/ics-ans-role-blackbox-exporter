# ics-ans-role-blackbox-exporter

Ansible role to install blackbox-exporter.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-blackbox-exporter
```

## License

BSD 2-clause
